package com.example.demo.controller;

import com.example.demo.model.Student;
import com.example.demo.service.StudentDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping("/v1")
public class DemoController {
    @Autowired
    StudentDataService studentDataService;
    @PostMapping("/create/student")
    public ResponseEntity<String> createStudent(@RequestBody Student student){
        studentDataService.createStudent(student);
        return ResponseEntity.ok("Student created successfully");

    }

    @GetMapping("/students")
    public ResponseEntity<String> writeStudentDataIntoFile() throws IOException {
        studentDataService.feedStudentDataInFile();
        return ResponseEntity.ok("Student fed into file successfully");

    }



}
