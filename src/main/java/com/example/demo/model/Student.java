package com.example.demo.model;

import lombok.Data;

@Data
public class Student {
    String name;
    String standard;
}
