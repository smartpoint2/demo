package com.example.demo.service;

import com.example.demo.entity.StudentEntity;
import com.example.demo.model.Student;
import com.example.demo.repository.StudentRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class StudentDataService {
    @Autowired
    StudentRepo studentRepo;


    public void createStudent(Student student) {
        StudentEntity studentEntity = new StudentEntity();
        studentEntity.setName(student.getName());
        studentEntity.setStandard(student.getStandard());
        studentRepo.save(studentEntity);
    }

    public void feedStudentDataInFile() throws IOException {
        List<StudentEntity> students = new ArrayList<>();
        students = studentRepo.findAll();
        long count = 0;
        String studentString = "";
        for (StudentEntity studentEntity : students) {
            if (count % 2 == 0) {
                writeToFile("student_"+count, studentString);
                studentString = "";
            }
            studentString += studentEntity.toString() + "\n";
            count++;
        }
        writeToFile("student_"+count, studentString);
    }

    private static void writeToFile(String fileName, String content) throws IOException {
        if(!content.equals("")) {
            BufferedWriter br = new BufferedWriter(new FileWriter(fileName + ".txt"));
            br.write(content);
            br.flush();
            br.close();
        }
    }

}
