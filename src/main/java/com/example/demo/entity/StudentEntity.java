package com.example.demo.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.Data;

@Data
@Entity(name="Student")
public class StudentEntity {
    @Id
    @GeneratedValue
    Long studentId;

    @Column(name="name")
    String name;

    @Column(name = "std")
    String standard;

    @Override
    public String toString(){
        return this.getStudentId()+","+this.getName()+","+this.getStandard();
    }

}
